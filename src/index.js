export function injectGroupBy() {
  const groupBy = function(argFunc = val => val) {
    return this.reduce((acc, val) => {
      const key = argFunc(val);
      acc[key] = [].concat(acc[key] || [], val);

      return acc;
    }, {});
  };

  Array.prototype.groupBy = groupBy;
}

export function quaddouble(num1 = 0, num2 = 0) {
  if (typeof num1 !== 'number' || typeof num2 !== 'number') {
    console.warn('[quaddouble] Both parameters must be numbers.'); // eslint-disable-line no-console
    return false;
  }

  const matches = num1.toString().match(/(\d)\1{3}/g);

  if (!matches) return false;

  return matches.reduce((acc, match) => {
    const exp = new RegExp(`(${match[0]}){2}`, 'g');

    return exp.test(num2.toString());
  }, false);
}

export function lispHero(inputStr) {
  const braces = new Map([
    ['(', ')'],
    ['[', ']'],
    ['{', '}'],
  ]);

  let matchFine;
  let str = inputStr;


  const matchFinder = function(closing, opening) {
    if (matchFine === false) return;

    const openingPos = str.lastIndexOf(opening);
    const closingRelPos = str.substr(openingPos + 1).indexOf(closing);

    // if both braces isn't there, count it as match
    if (!~openingPos && !~closingRelPos) {
      matchFine = true;
      return;
    }

    // if both braces are there, removing them from string and going recursive
    if (!!~openingPos && !!~closingRelPos) {
      str = `${str.substr(0, openingPos)}${str.substr(openingPos + 1, closingRelPos)}${str.substr(openingPos + closingRelPos + 2)}`;
      matchFinder(closing, opening);
      return;
    }

    // if we're here, there's an unmatching brace
    matchFine = false;
  };

  braces.forEach(matchFinder);

  return matchFine;
}
