import * as lib from './index';

lib.injectGroupBy();

describe('groupBy method', () => {
  test('must exist', () => {
    expect(typeof [].groupBy).toBe('function');
  });

  test('should group values if no function are given to it', () => {
    expect([1, 2, 3, 2, 4, 1, 5, 1, 6].groupBy()).toEqual({
      1: [1, 1, 1],
      2: [2, 2],
      3: [3],
      4: [4],
      5: [5],
      6: [6]
    });
  });

  test('should group values according to the given function', () => {
    expect([1, 2, 3, 2, 4, 1, 5, 1, 6].groupBy( val => val % 3 )).toEqual({
      0: [3, 6],
      1: [1, 4, 1, 1],
      2: [2, 2, 5 ]
    });

    expect([1, 2, 3, 2, 4, 1, 5, 1, 6].groupBy( val => val % 2 === 0 ? 'evens' : 'odds' )).toEqual({
      evens: [2, 2, 4, 6],
      odds: [1, 3, 1, 5, 1],
    });
  });
});

describe('quaddouble', () => {
  test('must exist', () => {
    expect(typeof lib.quaddouble).toBe('function');
  });

  test('should return false if one or more given values are not numbers', () => {
    expect(lib.quaddouble(false, 11223344)).toBe(false);
    expect(lib.quaddouble(2342342, '11223344')).toBe(false);
  });

  test('should return true for values (45568411115, 11223344)', () => {
    expect(lib.quaddouble(45568411115, 11223344)).toBe(true);
  });

  test('should return true for values (11112344445, 442253)', () => {
    expect(lib.quaddouble(11112344445, 442253)).toBe(true);
  });

  test('should return false for values (12222345, 123452)', () => {
    expect(lib.quaddouble(12222345, 123452)).toBe(false);
  });

  test('should return false for values (12345, 12345)', () => {
    expect(lib.quaddouble(12345, 12345)).toBe(false);
  });
});

describe('lispHero', () => {
  test('must exist', () => {
    expect(typeof lib.lispHero).toBe('function');
  });

  test('should count empty string as having matching braces', () => {
    expect(lib.lispHero('')).toBe(true);
  });

  test('should return true if all braces have a match', () => {
    expect(lib.lispHero('()')).toBe(true);
    expect(lib.lispHero('{}[]()')).toBe(true);
    expect(lib.lispHero('([{}])')).toBe(true);
  });

  test('should return false if there\'s an unmatch in braces', () => {
    expect(lib.lispHero('[)')).toBe(false);
    expect(lib.lispHero('())({}}{()][][')).toBe(false);
  });
});
